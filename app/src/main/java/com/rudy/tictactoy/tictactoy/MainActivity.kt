package com.rudy.tictactoy.tictactoy

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*
import java.util.*


class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        disappearBoard()
        initEmptyCells()
    }


    var player1 = ArrayList<Int>()
    var player2 = ArrayList<Int>()
    var emptyCells = ArrayList<Int>()
    var activePlayer = 1
    var gameMode = 1
    var winner = -1


    fun initEmptyCells() {
        emptyCells.clear()
        for(i in 1..9){
            emptyCells.add(i)
        }
    }

    fun restartGame(view:View) {
        button1.setBackgroundResource(android.R.drawable.btn_default)
        button2.setBackgroundResource(android.R.drawable.btn_default)
        button3.setBackgroundResource(android.R.drawable.btn_default)
        button4.setBackgroundResource(android.R.drawable.btn_default)
        button5.setBackgroundResource(android.R.drawable.btn_default)
        button6.setBackgroundResource(android.R.drawable.btn_default)
        button7.setBackgroundResource(android.R.drawable.btn_default)
        button8.setBackgroundResource(android.R.drawable.btn_default)
        button9.setBackgroundResource(android.R.drawable.btn_default)

        button1.text = ""
        button2.text = ""
        button3.text = ""
        button4.text = ""
        button5.text = ""
        button6.text = ""
        button7.text = ""
        button8.text = ""
        button9.text = ""

        initEmptyCells()
        player1.clear()
        player2.clear()
        activePlayer = 1

        button1.isEnabled = true
        button2.isEnabled = true
        button3.isEnabled = true
        button4.isEnabled = true
        button5.isEnabled = true
        button6.isEnabled = true
        button7.isEnabled = true
        button8.isEnabled = true
        button9.isEnabled = true
    }

    fun disappearBoard () {
        button1.visibility = View.GONE
        button2.visibility = View.GONE
        button3.visibility = View.GONE
        button4.visibility = View.GONE
        button5.visibility = View.GONE
        button6.visibility = View.GONE
        button7.visibility = View.INVISIBLE
        button8.visibility = View.INVISIBLE
        button9.visibility = View.INVISIBLE
        bRestart.visibility = View.INVISIBLE
    }

    fun showBoard () {
        button1.visibility = View.VISIBLE
        button2.visibility = View.VISIBLE
        button3.visibility = View.VISIBLE
        button4.visibility = View.VISIBLE
        button5.visibility = View.VISIBLE
        button6.visibility = View.VISIBLE
        button7.visibility = View.VISIBLE
        button8.visibility = View.VISIBLE
        button9.visibility = View.VISIBLE
        bRestart.visibility = View.VISIBLE
        bRestart.setBackgroundResource(R.drawable.button_shape)
    }

    fun buttonClick(view: View) {
        val buSelected:Button = view as Button
        var cellId = 0
        when(buSelected.id) {
            R.id.button1 -> cellId = 1
            R.id.button2 -> cellId = 2
            R.id.button3 -> cellId = 3

            R.id.button4 -> cellId = 4
            R.id.button5 -> cellId = 5
            R.id.button6 -> cellId = 6

            R.id.button7 -> cellId = 7
            R.id.button8 -> cellId = 8
            R.id.button9 -> cellId = 9
        }
        PlayGame(cellId,buSelected)
    }

    fun modeChoose(view:View) {
        val ps:Button = view as Button
        when(ps.id)
        {
            R.id.PVP -> {
                gameMode = 1
                ps.setBackgroundResource(R.drawable.button_shape)
                PVC.setBackgroundColor(android.R.drawable.btn_default)
                restartGame(view)
                showBoard()
            }
            R.id.PVC -> {
                gameMode = 2
                ps.setBackgroundResource(R.drawable.button_shape)
                PVP.setBackgroundColor(android.R.drawable.btn_default)
                restartGame(view)
                showBoard()
            }
        }
    }

    fun PlayGame(cellId:Int,buSelected:Button) {
        if (activePlayer == 1) {
            buSelected.text = "X"
            buSelected.setBackgroundResource(R.drawable.cell_shape_x)
            buSelected.setTextColor( getResources().getColor(R.color.yellow) )

            player1.add(cellId)
            emptyCells.remove(cellId)
            if(checkWinner() == -1) {
                activePlayer = 2
                if (gameMode == 2) {
                    try {
                        autoPlaySmart()
//                      autoPlay()
                    } catch (ex: Exception) {
                        Toast.makeText(this, "Draw!", Toast.LENGTH_SHORT).show()
                    }
                }
            }
        }
        else {
            buSelected.text = "O"
            buSelected.setBackgroundResource(R.drawable.cell_shape_o)
            buSelected.setTextColor( getResources().getColor(R.color.blue));

            player2.add(cellId)
            emptyCells.remove(cellId)
            winner = checkWinner()
            activePlayer = 1

        }
        if (emptyCells.size == 0){
            if (winner == -1){
                Toast.makeText(this, "Draw!", Toast.LENGTH_SHORT).show()
            }
        }
        buSelected.isEnabled = false
    }

    fun checkWinner():Int {
        winner = -1

        //row1
        if (player1.contains(1) && player1.contains(2) && player1.contains(3)) {
            winner = 1
        }
         else if (player2.contains(1) && player2.contains(2) && player2.contains(3)) {
            winner = 2
        }

        //row2
        else if (player1.contains(4) && player1.contains(5) && player1.contains(6)) {
            winner = 1
        }
        else if (player2.contains(4) && player2.contains(5) && player2.contains(6)) {
            winner = 2
        }

        //row3
        else if (player1.contains(7) && player1.contains(8) && player1.contains(9)) {
            winner = 1
        }
        else if (player2.contains(7) && player2.contains(8) && player2.contains(9)) {
            winner = 2
        }

        //col1
        else if (player1.contains(1) && player1.contains(4) && player1.contains(7)) {
            winner = 1
        }
        else if (player2.contains(1) && player2.contains(4) && player2.contains(7)) {
            winner = 2
        }

        //col2
        else if (player1.contains(2) && player1.contains(5) && player1.contains(8)) {
            winner = 1
        }
        else if (player2.contains(2) && player2.contains(5) && player2.contains(8)) {
            winner = 2
        }

        //col3
        else if (player1.contains(3) && player1.contains(6) && player1.contains(9)) {
            winner = 1
        }
        else if (player2.contains(3) && player2.contains(6) && player2.contains(9)) {
            winner = 2
        }

        //cross1
        else if (player1.contains(1) && player1.contains(5) && player1.contains(9)) {
            winner = 1
        }
        else if (player2.contains(1) && player2.contains(5) && player2.contains(9)) {
            winner = 2
        }

        //cross2
        else if (player1.contains(3) && player1.contains(5) && player1.contains(7)) {
            winner = 1
        }
        else if (player2.contains(3) && player2.contains(5) && player2.contains(7)) {
            winner = 2
        }

        if (winner != -1) {
            if (winner == 1) {
                if(gameMode == 1) {
                    Toast.makeText(this, "Player 1 Wins!!", Toast.LENGTH_SHORT).show()
                }
                else {
                    Toast.makeText(this, "You Won!!", Toast.LENGTH_SHORT).show()
                }
            }
            else {
                if (gameMode == 1) {
                    Toast.makeText(this, "Player 2 Wins!!", Toast.LENGTH_SHORT).show()
                }
                else {
                    Toast.makeText(this, "CPU Wins!!", Toast.LENGTH_SHORT).show()
                }
            }
            stopTouch()
        }
        return winner
    }

    fun stopTouch() {
        button1.isEnabled = false
        button2.isEnabled = false
        button3.isEnabled = false
        button4.isEnabled = false
        button5.isEnabled = false
        button6.isEnabled = false
        button7.isEnabled = false
        button8.isEnabled = false
        button9.isEnabled = false
    }

    fun autoPlay() {
        val emptyCells = ArrayList<Int>()
        for (cellId in 1..9) {
            if (!(player1.contains(cellId)) && !(player2.contains(cellId))) {
                emptyCells.add(cellId)
            }
        }

        val r = Random()
        val randomIndex = r.nextInt(emptyCells.size-0)+0
        val cellId = emptyCells[randomIndex]

        val buSelect:Button?
        when(cellId)
        {
            1 -> buSelect = button1
            2 -> buSelect = button2
            3 -> buSelect = button3
            4 -> buSelect = button4
            5 -> buSelect = button5
            6 -> buSelect = button6
            7 -> buSelect = button7
            8 -> buSelect = button8
            9 -> buSelect = button9
            else -> buSelect = button1
        }

        PlayGame(cellId,buSelect)
    }

    fun autoPlaySmart() {

        var cellId: Int? = null
        /////////////////////// try to win /////////////////////////////////////////////////////////
        //row1
        if(emptyCells.contains(1) || emptyCells.contains(2) || emptyCells.contains(3)){
            if (player2.contains(1) && player2.contains(2)){
                cellId = 3
            } else if (player2.contains(1) && player2.contains(3)){
                cellId = 2
            } else if (player2.contains(2) && player2.contains(3)){
                cellId = 1
            }
        }
        //row2
        if(emptyCells.contains(4) || emptyCells.contains(5) || emptyCells.contains(6)){
            if(player2.contains(4) && player2.contains(5)){
                cellId = 6
            } else if (player2.contains(4) && player2.contains(6)){
                cellId = 5
            } else if (player2.contains(5) && player2.contains(6)){
                cellId = 4
            }
        }
        //row3
        if(emptyCells.contains(7) || emptyCells.contains(8) || emptyCells.contains(9)){
            if(player2.contains(7) && player2.contains(8)){
                cellId = 9
            } else if (player2.contains(7) && player2.contains(9)){
                cellId = 8
            } else if (player2.contains(8) && player2.contains(9)){
                cellId = 7
            }
        }
        //col1
        if(emptyCells.contains(1) || emptyCells.contains(4) || emptyCells.contains(7)){
            if(player2.contains(1) && player2.contains(4)){
                cellId = 7
            } else if (player2.contains(1) && player2.contains(7)){
                cellId = 4
            } else if (player2.contains(4) && player2.contains(7)){
                cellId = 1
            }
        }
        //col2
        if(emptyCells.contains(2) || emptyCells.contains(5) || emptyCells.contains(8)){
            if(player2.contains(2) && player2.contains(5)){
                cellId = 8
            } else if (player2.contains(2) && player2.contains(8)){
                cellId = 5
            } else if (player2.contains(5) && player2.contains(8)){
                cellId = 2
            }
        }
        //col3
        if(emptyCells.contains(3) || emptyCells.contains(6) || emptyCells.contains(9)){
            if(player2.contains(3) && player2.contains(6)){
                cellId = 9
            } else if (player2.contains(3) && player2.contains(9)){
                cellId = 6
            } else if (player2.contains(6) && player2.contains(9)){
                cellId = 3
            }
        }
        //cross1
        if(emptyCells.contains(1) || emptyCells.contains(5) || emptyCells.contains(9)){
            if(player2.contains(1) && player2.contains(5)){
                cellId = 9
            } else if (player2.contains(1) && player2.contains(9)){
                cellId = 5
            } else if (player2.contains(5) && player2.contains(9)){
                cellId = 1
            }
        }
        //cross2
        if(emptyCells.contains(3) || emptyCells.contains(5) || emptyCells.contains(7)){
            if(player2.contains(3) && player2.contains(5)){
                cellId = 7
            } else if (player2.contains(3) && player2.contains(7)){
                cellId = 5
            } else if (player2.contains(5) && player2.contains(7)){
                cellId = 3
            }
        }
        if(cellId != null) {
            emptyCells.remove(cellId)
            checkWinner()
        } else {
//        if(checkWinner() == -1) {
            //////////////////////////// try not to lose //////////////////////
            //row1
            if (emptyCells.contains(1) || emptyCells.contains(2) || emptyCells.contains(3)) {
                if (player1.contains(1) && player1.contains(2)) {
                    cellId = 3
                } else if (player1.contains(1) && player1.contains(3)) {
                    cellId = 2
                } else if (player1.contains(2) && player1.contains(3)) {
                    cellId = 1
                }
            }
            //row2
            if (emptyCells.contains(4) || emptyCells.contains(5) || emptyCells.contains(6)) {
                if (player1.contains(4) && player1.contains(5)) {
                    cellId = 6
                } else if (player1.contains(4) && player1.contains(6)) {
                    cellId = 5
                } else if (player1.contains(5) && player1.contains(6)) {
                    cellId = 4
                }
            }
            //row3
            if (emptyCells.contains(7) || emptyCells.contains(8) || emptyCells.contains(9)) {
                if (player1.contains(7) && player1.contains(8)) {
                    cellId = 9
                } else if (player1.contains(7) && player1.contains(9)) {
                    cellId = 8
                } else if (player1.contains(8) && player1.contains(9)) {
                    cellId = 7
                }
            }
            //col1
            if (emptyCells.contains(1) || emptyCells.contains(4) || emptyCells.contains(7)) {
                if (player1.contains(1) && player1.contains(4)) {
                    cellId = 7
                } else if (player1.contains(1) && player1.contains(7)) {
                    cellId = 4
                } else if (player1.contains(4) && player1.contains(7)) {
                    cellId = 1
                }
            }
            //col2
            if (emptyCells.contains(2) || emptyCells.contains(5) || emptyCells.contains(8)) {
                if (player1.contains(2) && player1.contains(5)) {
                    cellId = 8
                } else if (player1.contains(2) && player1.contains(8)) {
                    cellId = 5
                } else if (player1.contains(5) && player1.contains(8)) {
                    cellId = 2
                }
            }
            //col3
            if (emptyCells.contains(3) || emptyCells.contains(6) || emptyCells.contains(9)) {
                if (player1.contains(3) && player1.contains(6)) {
                    cellId = 9
                } else if (player1.contains(3) && player1.contains(9)) {
                    cellId = 6
                } else if (player1.contains(6) && player1.contains(9)) {
                    cellId = 3
                }
            }
            //cross1
            if (emptyCells.contains(1) || emptyCells.contains(5) || emptyCells.contains(9)) {
                if (player1.contains(1) && player1.contains(5)) {
                    cellId = 9
                } else if (player1.contains(1) && player1.contains(9)) {
                    cellId = 5
                } else if (player1.contains(5) && player1.contains(9)) {
                    cellId = 1
                }
            }
            //cross2
            if (emptyCells.contains(3) || emptyCells.contains(5) || emptyCells.contains(7)) {
                if (player1.contains(3) && player1.contains(5)) {
                    cellId = 7
                } else if (player1.contains(3) && player1.contains(7)) {
                    cellId = 5
                } else if (player1.contains(5) && player1.contains(7)) {
                    cellId = 3
                }
            }
            /////////////////////////////////////////////////
        }

        if(cellId != null) {
            emptyCells.remove(cellId)
            checkWinner()
        } else {
            val r = Random()
            val randomIndex = r.nextInt(emptyCells.size-0)+0
            cellId = emptyCells[randomIndex]
            emptyCells.remove(cellId)
        }
        val buSelect:Button?
        when(cellId)
        {
            1 -> buSelect = button1
            2 -> buSelect = button2
            3 -> buSelect = button3
            4 -> buSelect = button4
            5 -> buSelect = button5
            6 -> buSelect = button6
            7 -> buSelect = button7
            8 -> buSelect = button8
            9 -> buSelect = button9
            else -> buSelect = button1
        }

        checkWinner()

        PlayGame(cellId,buSelect)
    }
}
